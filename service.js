const http = require('http');
const url = require('url');

const SERVER_PORT = (process.argv[2]) ? process.argv[2] : 5800
console.log("SERVER_PORT: ",SERVER_PORT);
http.createServer( async (req, response) => {
    const urlObj = url.parse(req.url);
    const urlArr = urlObj.pathname.split('/')
    console.log("urlArr: ",urlArr);
    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify({
        message: "Generate random number",
        data: Math.floor(Math.random() * 1000) + 1
    }));
}).listen(SERVER_PORT);